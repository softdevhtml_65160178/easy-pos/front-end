import type { Type } from '@/types/Type'
import http from './http'

function addType(type: Type) {
    return http.post('/types', type)
}

function updateType(type: Type) {
    return http.patch(`/types/${type.id}`, type)
}

function delType(type: Type) {
    return http.delete(`/types/${type.id}`)
}

function getType(id: number) {
    return http.get(`/types/${id}`)
}

function getTypes() {
    return http.get('/types')
}

export default { addType, updateType, delType, getType, getTypes }
